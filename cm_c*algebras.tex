%% Last revision on January 18, 2020.
%% Revised 04/29/15.
%% Notice that anything written on a line that begins 
%% with  a % sign is ignored by LaTeX.
\documentclass[12pt]{amsart}

\usepackage{enumitem}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{noname}[theorem]{}
\newtheorem{sublemma}{}[theorem]
\newtheorem{conjecture}[theorem]{Conjecture}

\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}

\theoremstyle{remark}
\newtheorem{remark}[theorem]{Remark}

\numberwithin{equation}{section}

\newcommand{\ba}{\backslash}
\newcommand{\utf}{Unicode Transformation Format}
\newcommand{\del}{\setminus}
\newcommand{\con}{\,/\,}
\newcommand{\unit}{1\!\!1}


\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Q}{\ensuremath{\mathbb{Q}}}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\C}{\ensuremath{\mathbb{C}}}


\allowdisplaybreaks

\begin{document}

\title{$C^*$-Algebras}

\author{Nicholas Christoffersen}
\address{Mathematics Department\\
  Louisiana State University\\
  Baton Rouge, Louisiana}
\email{nchri15@lsu.edu}

%\author{Thomas Jones}
%\address{School of Mathematical and Computing Sciences\\
%  Victoria University \\
%  Wellington\\ New Zealand}
%\email{tjones@kauri.vuw.ac.nz}
%
%\subjclass[2010]{05B35}
%\date{}

\begin{abstract}
  The purpose of $C^{*}$-algebra theory is to generalize the situation that describes the algebra of operators on a Hilbert space. These algebras carry both algebraic and analytic structure, which are intertwined via spectral theory. The goal of this expository paper is to show how the algebraic structure of a $C^{*}$-algebra completely determines its analytic structure. Specifically, we show that the norm on a $C^{*}$-algebra, an analytic concept, is determined by the spectrum, an algebraic concept.
\end{abstract}

\maketitle

\section{Introduction}
  \label{introduction}
  The goal of $C^{*}$-algebra theory when it began was to generalize the structure that appears when we consider the algebra of operators on a complex Hilbert space. In this paper, we begin by building up to the definition of a $C^{*}$-algebra, and introducing some results that are characteristic of and critical to the study of $C^{*}$-algebras. In Section 2, we define Hilbert spaces and provide prototypical examples of $C^{*}$-algebras, as well as make a connection between $C^{*}$-algebra theory and an introductory point-set topology result. Finally, we discuss the concept of a spectrum, and show how this, an algebraic concept, completely determines the analytic structure by defining a unique norm on the $C^{*}$-algebra, if one exists.

  \begin{definition}
      A vector space $\mathcal{A}$ over a field $\mathbb{F}$ is an \textit{algebra} if it is equipped with a multiplication, $(A,B)\mapsto AB$, such that, for all $A,B,C\in \mathcal{A}$ and $\alpha, \beta\in \mathbb{F}$,
      \begin{enumerate}
          \item $A(BC) = (AB)C$;
          \item $A(B+C) = AB + AC$; and
          \item $\alpha\beta(AB) = (\alpha A)(\beta B)$.
      \end{enumerate}
      We say that $\mathcal{A}$ is \textit{abelian}, or commutative, if the multiplication is commutative:
      \begin{align*}
          AB = BA,
      \end{align*}
      for all $A,B\in \mathcal{A}$.

      Further, we say that $\mathcal{A}$ is \textit{unital}, or has an identity, if there exists an element $\unit\in \mathcal{A}$ such that for all $A\in \mathcal{A}$,
      \[
          \unit A = A \unit = A.
      \]
  \end{definition}

  Good examples of algebras are $C^{r}(I)$, the set of $r$-continuously differentiable functions on the unit interval in $\R$, and the set of $n\times n$ matrices over a field $\mathbb{F}$, denoted $\text{M}_n(\mathbb{F})$. 
  The second of these algebras is particularly interesting when $\mathbb{F} = \C$,  it has an involution.

  \begin{definition}
      Let $\mathcal{A}$ be an algebra over $\C$ or $\R$. A map $^{*}:\mathcal{A}\to \mathcal{A}$, $A\mapsto A^*$ is called an \textit{involution}, or adjoint operation on $\mathcal{A}$, if for all $A, B\in \mathcal{A}$, and $\alpha, \beta\in \C$, we have
      \begin{enumerate}
          \item $A^{**} = A$;
          \item $(AB)^* = B^*A^*$; and 
          \item $(\alpha A + \beta B)^* = \overline{\alpha}A^* + \overline{\beta}B^*$,
      \end{enumerate}
      where $\overline{\alpha}$ denotes the conjugate of $\alpha\in \C$ (or $\overline{\alpha} = \alpha$, if $\alpha\in \R$).
  \end{definition}

  The simplest example of an involutive algebra is $\C$, the complex numbers, viewed as an algebra over itself. Involution in $\C$ is given by taking conjugates. In the case of $\text{M}_n(\C)$, the involution of a matrix $A$ is given by taking the adjoint (conjugate transpose) of $A$, denoted $A^{*}$, or sometimes $A^{\dagger}$. In the special case of $\text{M}_n(\R)$, the involution is given by taking the transpose.

  Now, to do analysis on a space, it is helpful to have a notion of size, or norm.

  \begin{definition}
      An algebra $\mathcal{A}$ is a \textit{normed algebra} if there is a map 
      \begin{align*}
          A \mapsto \|A\|\in \R_+
      \end{align*}
      that sends $A$ to the norm of $A$ and has the following properties:
      \begin{enumerate}
          \item $\|A\| \geq 0$ and $\|A\| = 0$ if and only if $A = 0$;
          \item $\|\alpha A\| = \left| \alpha \right| \|A\|$; 
          \item $\| A + B \| \leq \|A\| + \|B\|$; and
          \item $\|AB\|\leq \|A\|\|B\|$. 
      \end{enumerate}
      The third and fourth of these properties are called the \textit{triangle inequality} and \textit{product inequality}, respectively.

      This norm defines a topology, which is called the \textit{uniform topology}. If a normed algebra is complete with respect to its uniform topology, it is called a \textit{Banach algebra}. If, in addition, it is an involutive algebra with the property that $\|A\| = \|A^{*}\|$, then it is called a \textit{Banach $*$-algebra}.
  \end{definition}

  \begin{definition}
      A involutive Banach algebra $\mathcal{A}$ is a \textit{$C^{*}$-algebra} if the following property, called the $C^{*}$ identity, is satisfied:
      \[
          \|A^{*}A\| = \|A\|^2.
      \]
  \end{definition}

  In all that follows, $\mathcal{A}$ denotes a $C^{*}$-algebra, possibly without identity. However, the existence of an identity element is quite useful. To address this, we have the following result.

  \begin{theorem}\label{add-identity}
      Let $\mathcal{A}$ be a non-unital $C^{*}-$algebra, and $\tilde{\mathcal{A}}$ denote the algebra of pairs $\{(\alpha, A): \alpha\in \C, A\in \mathcal{A}\} $ with
      \begin{align*}
          (\alpha, A) + (\beta, B) &= (\alpha + \beta, A + B),\\
          (\alpha, A)(\beta, B) &= (\alpha\beta, \alpha B + \beta A + AB), \text{ and }\\
          (\alpha, A)^{*} &= (\overline{\alpha}, A^{*}).
      \end{align*}
      With the following norm
      \[
          \|(\alpha, A)\| = \sup \{\|\alpha B + AB\|: B\in \mathcal{A}, \|B\| = 1\},
      \]
      $\tilde{\mathcal{A}}$ is a $C^{*}$-algebra with identity. 
  \end{theorem} 

  \begin{remark}
    We will refer to $\tilde{\mathcal{A}}$ as adjoining the identity to $\mathcal{A}$. We can identify the $\mathcal{A}$ as the sub-$C^{*}$-algebra $\{(0, A)\in \tilde{\mathcal{A}}\} $. In other words, if we have an algebra $\mathcal{A}$, we can assume that it is contained in a larger \textit{unital} algebra $C^{*}$-algebra. We will return to this result when we have the examples to make it meaningful.
  \end{remark}
  
    
  %\begin{definition}
  %    We say that $A\in \mathcal{A}$ is \textit{normal} if
  %    \[
  %        A A^{*} = A^{*} A,
  %    \]
  %    and selfadjoint if
  %    \[
  %        A = A^{*}.
  %    \]
  %    If $\mathcal{A}$ has an identity \unit, then $A$ is called an isometry whenever
  %    \[
  %        A^{*}A = \unit,
  %    \]
  %    and $A$ is unitary if
  %    \[
  %        A^* A = A A^* = \unit.
  %    \]
  %\end{definition}

  %\section{Hilbert Space}
  %The motivation behind the theory of $C^{*}$-algebras was to generalize the idea of bounded linear operators acting on a complex Hilbert space. So that we may draw from a wealth of examples

  %\begin{definition}
  %    A complex \textit{Hilbert space} $\mathcal{H}$ is a complete inner product space. That is, a linear space (i.e. vector space) over $\C$ equipped with an inner product
  %    \[
  %        \left<\cdot, \cdot \right> : \mathcal{H} \times \mathcal{H} \to \C
  %    \]
  %    such $\mathcal{H}$ is complete with resepct to the norm induced by this inner product ($\|A\| := \sqrt[]{\left<A,A \right>}$).
  %\end{definition}

  %\begin{definition}
  %    A bounded linear operator on a Hilbert Space $\mathcal{H}$ is a linear map $A$,
  %    \[
  %        A: \mathcal{H} \to  \mathcal{H}
  %    \]
  %    such that it is bounded in the operator norm
  %    \[
  %        \|A\| := \sup \{\|A\xi\| : \|\xi\| = 1\} < \infty.
  %    \]
  %    The set of all such operators is denoted $\mathcal{L}(\mathcal{H})$.
  %\end{definition}

  %The set $\mathcal{L}(\mathcal{H})$ is a $C^{*}$-algebra. Scalar multiplication and addition are defined pointwise, and multiplication by composition:
  %\[
  %    (A + B)x = Ax + Bx; \quad (AB)x = A(Bx), \text{ for all $A, B\in \mathcal{L}(\mathcal{H})$}
  %\]
  %Indeed, it is a normed linear space, and one quickly verifies that it is complete. Moreover, they are very general examples of $C^{*}$-algebras. Indeed, one main result of $C^{*}$-algebra theory is that every $C^{*}$-algebra can be represented by a direct sum of $C^{*}$-algebras of this type.

  %With some Hilbert space theory built, we can consider some interesting examples:

  %\ldots

  \section{Examples of $C^{*}$-algebras}

  Recall from the introduction that the theory of $C^{*}$-algebras came from a desire to generalize the notion of the space of operators on a Hilbert space. In this section, we give a few examples of $C^{*}$-algebras. 

  \begin{definition}
      A complex \textit{Hilbert space} $\mathcal{H}$ is a complete inner product space, that is, a vector space over $\C$ equipped with an inner product
      \[
          \left<\cdot, \cdot \right> : \mathcal{H} \times \mathcal{H} \to \C
      \]
      such $\mathcal{H}$ is complete with respect to the norm induced by this inner product ($\|\xi\| := \sqrt[]{\left<\xi,\xi \right>}$).
  \end{definition}

  \begin{definition}
      A bounded linear operator on a Hilbert Space $\mathcal{H}$ is a linear map $A$,
      \[
          A: \mathcal{H} \to  \mathcal{H}
      \]
      such that it is bounded in the operator norm
      \[
          \|A\| := \sup \{\|A\xi\| : \|\xi\| = 1\} < \infty.
      \]
      The set of all such operators is denoted $\mathcal{L}(\mathcal{H})$.
  \end{definition}

  We now give our first example of a $C^{*}$-algebra.

  \begin{example}
      Consider $M_{n}(\C)$, the set of $n\times n$ matrices over the complex numbers. With taking adjoints as the involution, and under the operator norm:
      \[
          \|A\| = \sup \{\|Ax\| : x\in \C^{n}, \|x\| = 1\} ,
      \]
      the space $M_{n}(\C)$ forms a $C^{*}$-algebra. Note that this is a special instance of  a family of operators on a Hilbert space.
  \end{example}

  As one can see from the previous example, $C^{*}$-algebras can, in general, be non-abelian. However, the study of abelian $C^{*}$-algebras is important, as many results can be proved by proving them in an abelian sub-$C^{*}$-algebra of the original space. To understand a very important example of an abelian $C^{*}$-algebra, we first need the following:

  \begin{definition}\label{def:vanish-at-infinity}
      Let $f:X \to \C$ be a continuous map from a topological space $X$ to $\C$. We say that $f$ \textit{vanishes at infinity} if, for every $\epsilon > 0$, there exists a compact set $K$ such that
      \[
          |f(x)| < \epsilon, \text{ for all } x\in X\setminus K.
      \]
  \end{definition}

  \begin{example}\label{ex:c0X}
      Let $X$ be a topological space. Consider the set of continuous functions from $X$ to $\C$ that vanish at infinity, denoted $C_0(X)$. Under the supremum norm:
      \[
          \|f\| = \sup \{|f(x)| : x\in X\},
      \]
      and with involution given pointwise, that is, $f^{*}(x) = \overline{f(x)}$, the space $C_0(X)$ forms an abelian $C^{*}$-algebra. Further, it has identity if and only if $X$ is compact.
  \end{example}

  The preceding example not only gives us an abelian $C^{*}$-algebra, but also one possibly without unity. Recall Theorem \ref{add-identity}, where we stated that to any $C^{*}$-algebra, we may add identity. In the case of $C_0(X)$, where $X$ is not compact, this is equivalent taking the one-point compactification of $X$, denoted $\tilde{X}$, and viewing $C_0(X)$ as a sub-$C^{*}$-algebra of $C_0(\tilde{X})$. %\textit{(Note to James Oxley, one-point compactification is something that we explicitly covered in Topology I, so I felt comfortable not putting in a definition, since we are writing to 2nd semester graduate students. Let me know if you would prefer it be included)}


   \section{Spectral Theory}

 Spectral theory is a broad area of study that is used in a variety of mathematics. The spectrum can be defined for a variety of objects, in particular, it can be described for operators. In the finite dimensional case, the elements of the spectrum are referred to as \textit{eigenvalues}. The eigenvalues of a matrix $A$ are given by those elements $\lambda\in \C$ for which there is a non-zero vector $v$ such that $Av = \lambda v$. To generalize this concept to operators on an infinite dimensional space, we must generalize the notion of the spectrum to be all values $\lambda $ such that $A - \lambda I$ is not invertible. In particular, $A - \lambda I$ may not be invertible if it is not injective, or if it is not surjective. Considering these cases give us a decomposition of the spectrum into different parts. We will not discuss this here, but it is an interesting topic of a graduate functional analysis course.

 \begin{definition}
     Let $\mathcal{A}$ have identity \unit. We say that $A\in \mathcal{A}$ is \textit{invertible} if there is an element $B\in \mathcal{A}$ such that $AB = BA = \unit$
 \end{definition}
  
 \begin{definition}
     Let $\mathcal{A}$ have identity $\unit$. The \textit{spectrum} of an element $A\in \mathcal{A}$ (in the algebra $\mathcal{A}$) is
      \[
          \sigma_{\mathcal{A}}(A):=\{\lambda\in \C \mid \lambda \unit - A \text{ is not invertible (in $\mathcal{A}$)}\} .
      \]
      The complement of $\sigma_{\mathcal{A}}(A)$ in $\C$ is called the \textit{resolvent set}, and is denoted $r_{\mathcal{A}}(A)$.
  \end{definition}

  \begin{example}
      Recall Example \ref{ex:c0X}, the $C^{*}$-algebra $C_0(X)$ of continuous, complex-valued functions on $X$ that vanish at infinity. We now consider $C_0(I)$
      where $I = [0,1]\subseteq \R$. Note that the condition that each function vanishes at infinity is no longer needed in this specific example, since the whole space, $I$, is itself compact. (Thus in Definition \ref{def:vanish-at-infinity}, for every $\epsilon > 0$, take $K = I$, and the condition is satisfied.) Further, $C_0(I)$ has identity, namely the function $\unit(x) \equiv 1$. 

      Now, let $f\in C_0(I)$. The function $f - \lambda \unit$ is invertible if and only if $(f - \lambda \unit)(x) \neq 0$ for all $x\in I$. Therefore, $\lambda\in \sigma_{C_0(I)}(f)$ if and only if $\lambda\in f(I)$, so that $\sigma_{C_0(I)}(f) = f(I)$. The spectrum of $f$ is the range of $f$ in the case of $C_0(I)$.
  \end{example}

  \begin{remark}
      This definition depends on which algebra we consider $A$ to be an element of. For example, it seems possible that $A$ belongs to two algebras, and that an inverse of $\lambda \unit - A$ lies in one of these algebras but not the other. We will not go into this here, but fortunately this is not true in the case of $C^{*}-$algebras (but may be the case in general Banach algebras). The spectrum of an element of a $C^*$-algebra is independent of which $C^*$-algebra one considers it an element of. This motivates the following:
  \end{remark}

  \begin{definition}
      Let $\mathcal{A}$ be without identity. Then we define the spectrum of $A\in \mathcal{A}$ as
      \[
          \sigma_{\mathcal{A}}(A) := \sigma_{\tilde{\mathcal{A}}}(A).
      \]
  \end{definition}

  The following proposition is an important step in understanding how spectra behave in general.

  \begin{proposition}
      The spectrum of an element $A\in \mathcal{A}$ is a closed set.
  \end{proposition}
  \begin{proof}
      First, assume that $\mathcal{A}$ has identity, by adjoining one if necessary. We will show that the resolvent is open. So let $\lambda_0\in \C\setminus \sigma(A)$. By definition, this means that $(\lambda_0\unit - A)$ is invertible. Then, for all $\lambda$ with $\left| \lambda - \lambda_0 \right| < \|(\lambda_0\unit - A)^{-1}\|$, define 
      \[
          B_\lambda = \sum_{m\geq 0}^{} (\lambda_0 - \lambda)^{m}(\lambda_0\unit - A)^{-m -1}.
      \]
      Since $\mathcal{A}$ is a $C^{*}$-algebra, it is, in particular, a Banach algebra, so that this limit is well defined (since the sequence is Cauchy). We will show that $B_\lambda$ is an inverse for $(\lambda\unit - A)$, so that $\lambda$ is also in the resolvent set of $A$. We calculate
      \begin{align*}
          B_{\lambda}(\lambda \unit - A) &= \sum_{m\geq 0}^{} (\lambda_0 - \lambda)^{m}(\lambda_0\unit - A)^{-m -1}(\lambda \unit - A)\\
                                         &= \sum_{m\geq 0}^{} (\lambda_0 - \lambda)^{m}(\lambda_0\unit - A)^{-m -1}((\lambda - \lambda_0 + \lambda_0) \unit - A)\\
                                         &= \sum_{m\geq 0}^{} (\lambda_0 - \lambda)^{m}(\lambda_0\unit - A)^{-m -1}(\lambda_0 \unit - A)\\
                                         & \quad + \sum_{m\geq 0}^{} (\lambda_0 - \lambda)^{m}(\lambda_0\unit - A)^{-m -1}(\lambda - \lambda_0)\unit\\
                                         &= \sum_{m\geq 0}^{} (\lambda_0 - \lambda)^{m}(\lambda_0\unit - A)^{-m}\\
                                         & \quad - \sum_{m\geq 0}^{} (\lambda_0 - \lambda)^{m + 1}(\lambda_0\unit - A)^{-m -1}\\
                                         &= \sum_{m\geq 0}^{} (\lambda_0 - \lambda)^{m}(\lambda_0\unit - A)^{-m}\\
                                         & \quad - \sum_{m\geq 1}^{} (\lambda_0 - \lambda)^{m}(\lambda_0\unit - A)^{-m}\\
                                         &= (\lambda_0 - \lambda)^{0}(\lambda_0\unit - A)^{0} = \unit
      .\end{align*}
      So $B_\lambda$ is indeed a left inverse for $\lambda \unit - A$. A similar calculation shows that it is a right inverse, and thus $\lambda \unit - A$ is invertible. Therefore, $\lambda\in \C\setminus \sigma(A)$, so that $\C\setminus \sigma(A)$ is open.
  \end{proof}

  The preceding result and proof are quite representative of other proofs when one begins studying spectral theory. The completeness of $\mathcal{A}$ allows one to define these series, which are designed to be elements of $\mathcal{A}$ that one tries to construct. Even more interesting (at least to this author) is that integrals of $C^{*}$-algebra valued functions can be well-defined. Indeed, when studying the functional calculus on $C^{*}$-algebras, one tries to make sense of extending functions to be valued in a $C^{*}$-algebra. As the reader has just seen, polynomials, and even analytic functions, of $C^{*}$-algebras can be well-defined by power series. Moreover, one can use these, along with integral definitions, to define continuous functions of $C^{*}$-algebras. One very important example of this is to define the square root of an element of a $C^{*}$-algebra.

  Now, we begin to discuss the interplay between spectrum, an algebraic concept, and the norm, an analytic concept. For this, we need to define the spectral radius of an element of a $C^{*}$-algebra.

  \begin{definition}
      Let $A$ be an element of a Banach algebra with identity. Define the \textit{spectral radius} $\rho(A)$ of $A$ by
      \[
          \rho(A) = \sup \{\left| \lambda \right|, \lambda\in \sigma(A)\}.
      \]
  \end{definition}

  So far, everything still looks entirely algebraic. Then, one considers the following result, and can begin to see the interplay between the two.

  \begin{proposition}
      The spectral radius of $A\in \mathcal{A}$ is given by
       \[
           \rho(A) = \lim_{n \to \infty} \|A^{n}\|^{1 / n} = \inf_{n}\|A^{n}\|^{1 / n}.
      \]
  \end{proposition}

  \begin{definition}
      An element $A\in \mathcal{A}$ is called \textit{normal} if
      \[
          A^* A = A A^*.
      \]
  \end{definition}
  
  Finally, we come to the main result.

 \begin{theorem}
      Let $\mathcal{A}$ be a $C^{*}$-algebra with identity. If $A\in \mathcal{A}$ is a normal element, then the spectral radius, $\rho(A)$, of $A$ is given by
      \begin{align*}
          \rho(A) = \|A\|.
      \end{align*}
  \end{theorem}

  \begin{corollary}
      If $\mathcal{A}$ is a $*$-algebra and there exists a norm on $\mathcal{A}$ with the $C^{*}$-norm property, and $\mathcal{A}$ is closed with respect to this norm, then this norm is unique.
  \end{corollary}
  
 
  
 \section*{Acknowledgements}
  The author thanks Dr.~Gestur \'Olafsson for his help and support throughout the writing of this work, and Dr.~James Oxley for his comments on clarity and presentation.
\begin{thebibliography}{99}

  \bibitem{brat-rob} Bratelli, Ola and Robinson, Derek W., \textit{Operator Algebras and Quantum Statistical Mechanics: $C^{*}$- and $W^{*}$-Algebras Symmetry Groups Decompositions of States}, Second edition, Springer-Verlag, New York, 1987.

  %\bibitem{pjc} Cameron, P.J., Finite geometries, in \textit{Handbook of Combinatorics}
  %  (eds. R. Graham, M. Gr\"{o}tschel, and L. Lov\'asz), Elsevier, Amsterdam; MIT Press,
  %  Cambridge, 1995, pp. 647--691.

  %\bibitem{jpsk2} Kung, J.P.S., Extremal matroid theory, in \textit{Graph Structure Theory}
  %  (eds. N. Robertson  and P. Seymour), \textit{Contemporary Mathematics}
  %  \textbf{147}, Amer. Math. Soc., Providence, 1993, pp. 21--61.

  %\bibitem{jpsk} Kung, J.P.S., Critical problems, in \textit{Matroid Theory}
  %  (eds. J. E. Bonin, J.G. Oxley, and B. Servatius), \textit{Contemporary Mathematics}
  %  \textbf{197}, Amer. Math. Soc., Providence, 1996, pp. 1--127.

  %\bibitem{ox1} Oxley, J.G., \textit{Matroid Theory}, Oxford University Press, New York, 1992.

  %\bibitem{wh2} Whittle, G.P., $q$-lifts of tangential $k$-blocks,  
  %  \textit{J. London Math. Soc. (2)} \textbf{39} (1989), 9--15.

\end{thebibliography}

\end{document}


