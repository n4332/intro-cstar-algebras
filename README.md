# A Brief Introduction to C\*-algebras

The above is a paper to match a persentation I gave in the Spring of 2021 at LSU, as part of a first-year communicating mathematics course.

A pdf of the current LaTeX code is available [here](https://gitlab.com/n4332/intro-cstar-algebras/-/jobs/artifacts/master/raw/cm_c*algebras.pdf?job=compile-tex) and [here (san serif for dyslexic accessibility)](https://gitlab.com/n4332/intro-cstar-algebras/-/jobs/artifacts/master/raw/cm_c*algebras_dyslexic.pdf?job=compile-tex-dyslexic)
